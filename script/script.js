/*Objetivos:
  Ler uma quantidade qualquer de palavras até escrever Sair
  Retornar quantas vezes cada palavra foi digitada
  qual o total de caracteres digitados
  qual o caractere mais digitado*/

 (function(){
    //objetos de controle
    function RegistroModel(valor, quantidade) {
        this.valor = valor;
        this.quantidade = quantidade;
    }
     

    //variáveis para manipular o dom
    var btnCadastrar = document.getElementById('btn-cadastrar');
    var iptPalavra = document.getElementById('ipt-palavra');
    var tabelaPalavras = document.querySelector('#tbl-palavras tbody');
    var tabelaCaracteres = document.querySelector('#tbl-caracteres tbody');
    var lblPalavras = document.getElementById('lbl-palavras');
    var lblPalavraMaisDigitada = document.getElementById('lbl-palavra-mais-digitada');
    var lblCaracteres = document.getElementById('lbl-caracteres');
    var lblCaractereMaisDigitado = document.getElementById('lbl-caracter-mais-digitado');

    // arrays
    var colecaoPalavras = [];   
    var colecaoLetras = [];

    btnCadastrar.addEventListener("click", e =>{
        criticaPalavraNaoPreenchida();
        if (iptPalavra.value.toUpperCase() == 'SAIR'){                  
            popularTabela(colecaoPalavras, tabelaPalavras);
            popularTabela(colecaoLetras, tabelaCaracteres);
            focaControle(iptPalavra);          
        }else{                
            cadastraRegistro(colecaoPalavras, iptPalavra.value);
            cadastraCaracter(iptPalavra.value); 
            ordenaRegistros(colecaoPalavras);
            ordenaRegistros(colecaoLetras);
            calculaResultados();
            focaControle(iptPalavra); 
        } 
    })

    function focaControle(pControle){
        pControle.value = '';
        pControle.focus();      
    }
    function ordenaRegistros(colecao){
        colecao.sort(function(a,b) {return parseInt(b.quantidade) - parseInt(a.quantidade)});    
    }

    function criticaPalavraNaoPreenchida(){
        if (iptPalavra.value == ''){
            alert('Necessário digitar um nome');
            iptPalavra.focus();
            return(false);
        }
    }    

    function cadastraRegistro(colecao, valor){
        var i = colecao.findIndex(palavra => palavra.valor.toUpperCase() == valor.toUpperCase());
        if (i == -1){             
            colecao.push(new RegistroModel(valor, 1));            
        }else{
            colecao[i].quantidade = parseInt(colecao[i].quantidade) + 1;
        } 
    }

    function cadastraCaracter(palavra){
        palavra = palavra.replace(/ /g,"")
        for (let i = 0; i < palavra.length; i++) {
            cadastraRegistro(colecaoLetras,  palavra[i]);                    
        }            
    }

    function popularTabela(colecao, tabela){
        var linha = document.createElement('tr');
        var coluna = document.createElement('td');
        
        for (let i = 0; i < colecao.length; i++) {
            linha = document.createElement('tr');
            coluna = document.createElement('td');
            
            const registro = colecao[i];                            
            coluna.textContent = registro.valor;
            linha.appendChild(coluna);

            coluna = document.createElement('td');
            coluna.textContent = registro.quantidade;
            linha.appendChild(coluna);
            tabela.appendChild(linha);                       
        }        
    }

    function calculaResultados(){
        lblPalavras.textContent = calcularValoresDigitados(colecaoPalavras);
        lblCaracteres.textContent = calcularValoresDigitados(colecaoLetras); 
        lblPalavraMaisDigitada.textContent =  retornarRegistroMaisDigitado(colecaoPalavras);
        lblCaractereMaisDigitado.textContent =  retornarRegistroMaisDigitado(colecaoLetras);
    }

    function calcularValoresDigitados(colecao){
        var quantidadeRegistros = 0;
        for (const registro of colecao) {
            quantidadeRegistros =  quantidadeRegistros + parseInt(registro.quantidade);               
        }    
        return quantidadeRegistros;
    }

    function retornarRegistroMaisDigitado(colecao){
        var lRegistroMaisDigitado = '';
        var quantidadeRegistro = 0;
        for (let i = 0; i < colecao.length; i++) {
            if (parseInt(colecao[i].quantidade) > quantidadeRegistro){
                lRegistroMaisDigitado = colecao[i].valor;
                quantidadeRegistro = colecao[i].quantidade;
            }            
        }
        return lRegistroMaisDigitado;
    }
 //COLOCAR AQUI TODO O CÓDIGO
})()
    